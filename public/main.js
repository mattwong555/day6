(function () {
        var CartApp = angular.module("CartApp", []);

        var CartCtrl = function () {
            var cartCtrl = this;

            cartCtrl.item = "";
            cartCtrl.quantity = 0;
            cartCtrl.basket = [];

            cartCtrl.addToBasket = function () {
                // add item to basket

             
                var idx = cartCtrl.basket.findIndex(function(elem) {
                    return (elem.item == cartCtrl.item);

                });
                if (-1 == idx)
                    cartCtrl.basket.push({
                        item: cartCtrl.item,
                        quantity: cartCtrl.quantity



                    });

                else 
                cartCtrl.basket[idx].quantity += cartCtrl.quantity;

                //reset the model
                cartCtrl.item = "";
                cartCtrl.quantity = 0;
            






        };

    }

    CartApp.controller("CartCtrl", [CartCtrl]);


})();